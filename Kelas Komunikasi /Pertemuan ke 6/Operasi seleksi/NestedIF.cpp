#include <iostream>
using namespace std;

int main()
{
    int angka;

    cout << "Masukkan nilai Bilangan Bulat: ";
    cin >> angka;

    // kondisi (IF) diluar
    if (angka != 0)
    {

        // Kondisi (IF) didalam
        if (angka > 0)
        {
            cout << "Angka positive." << endl;
        }
        // Kondisi (Else) didalam
        else
        {
            cout << "Angka negative." << endl;
        }
    }
    //Kondisi (Else) diluar
    else
    {
        cout << "Angka 0 dan  diantara bilangan positive dan negative." << endl;
    }

   
    return 0;
}