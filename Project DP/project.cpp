#include <iostream> //import input dan output
#include <string>
#include <fstream> //membaca data dan file

// fungsi std
using namespace std;

const int maxRow = 5; // jumlah data yang simpan

// membuat variable Nama dan ID
string EmpID[maxRow] = {};
string EmpNama[maxRow] = {};

// fungsi membaca file
void membukaFile()
{
    string line;
    // ifstream fileSaya("D:\\DataMahasiswa.txt"); //ini untuk sistem operasi windows
    ifstream fileSaya("/home/cryptography/Documents/0.1  Dosen UTS 2021/MK Dasar Pemrograman/c/Project DP/DataMahasiswa.txt"); // ini untuk Linux
    if (fileSaya.is_open())
    {
        int a = 0;
        while (getline(fileSaya, line))
        {
            int l = line.length();
            EmpID[a] = line.substr(0, 3);
            EmpNama[a] = line.substr(4, 1 - 4);
            a++;
        }
    }
    else
    {
        cout << "Berhasil membuka file " << endl;
    }
}

// membuat fungsi tambah data mahasiswa
void TambahDataMahasiswa()
{
    // tipe data char Nama dan Number
    char nama[50];
    char empNo[10];

    cin.ignore();              // agar ID mhs dan nama mhs bisa ke enter
    cout << "ID Mahasiswa:";   // mencetak di layar ID Mahasiswa
    cin.getline(empNo, 10);    // inputan nilai ID Mahasiswa max 10
    cout << "Nama Mahasiswa:"; // mencetak di layar Nama Mahasiswa
    cin.getline(nama, 50);     // inputan nilai Nama Mahasiswa max 15

    // looping (perulangan)
    for (int i = 0; i < maxRow; i++)
    {
        if (EmpID[i] == "\0")
        {

            EmpID[i] = empNo;
            EmpNama[i] = nama;

            cout << "Data Mahasiswa Berhasil ditambah!" << endl;
            break;
        }
    }
}

// membuat fungsi baca data mahasiswa
void BacaDataMahasiswa()
{
    system("clear");
    cout << "================================" << endl;
    cout << "=====Membaca Data Mahasiswa=====" << endl;
    cout << "================================" << endl;

    int counter = 0;
    cout << "No.      |      ID     |      NAMA     |" << endl
         << "---------------------------------------\n";
    for (int j = 0; j < maxRow; j++)
    {
        if (EmpID[j] != "\0")
        {
            counter++;
            cout << "   " << counter << "        " << EmpID[j] << "       " << EmpNama[j] << endl;
        }

        else if (counter == 0)
        {
            cout << "Data Mahasiswa masih kosong, silahkan inputkan data \n"
                 << endl;

            break;
        }
    }
}

// membuat fungsi ubah data mahasiswa
void UbahDataMahasiswa(string mencari)
{
    // tipe data char Nama dan Number
    char nama[50];
    char empNo[10];

    int counter = 0;

    for (int z = 0; z < maxRow; z++)
    {
        if (EmpID[z] == mencari)
        {
            counter++;

            cout << "Nama Mahasiswa:";
            cin.getline(nama, 50);

            EmpNama[z] = nama;

            cout << "Ubah Data Mahasiswa Berhasil!" << endl;
            break;
        }
    }

    if (counter == 0)
    {
        cout << "ID Mahasiswa tidak ditemukan" << endl;
    }
}

// membuat fungsi hapus data mahasiswa
void HapusDataMahasiswa(string mencari)
{
    // membuat variable counter
    int counter = 0;

    for (int z = 0; z < maxRow; z++)
    {
        if (EmpID[z] == mencari)
        {
            counter++;

            EmpNama[z] = "";
            EmpID[z] = "";

            cout << "Data Mahasiswa Berhasil dihapus!" << endl;
            break;
        }
    }

    if (counter == 0)
    {
        cout << "ID Mahasiswa tidak ditemukan" << endl;
    }
}

// fungsi mencari data mahasiswa
void CariDataMahasiswa(string mencari)
{
    int counter = 0;
    system("clear");
    cout << "=======================================" << endl;
    cout << "=============Data saat ini=============" << endl;
    cout << "=======================================" << endl;

    cout << "No.      |      ID     |      NAMA     |" << endl
         << "---------------------------------------\n";
    for (int x = 0; x < maxRow; x++)
    {

        if (EmpID[x] == mencari)
        {
            counter++;
            cout << "   " << counter << "         " << EmpID[x] << "         " << EmpNama[x] << endl;
            break;
        }
    }
    if (counter == 0)
    {
        cout << "Data Mahasiswa tidak ditemukan\n"
             << endl;
    }
}

// fungsi menyimpan file
void menyimpanFile()
{
    ofstream fileSaya;
    // fileSaya.open("D:\project"); //for windows
    fileSaya.open("/home/cryptography/Documents/0.1  Dosen UTS 2021/MK Dasar Pemrograman/c/Project DP/DataMahasiswa.txt");

    // membuat perulangan
    for (int i = 0; i < maxRow; i++)
    {
        if (EmpID[i] == "\0")
        {
            break;
        }
        else
        {
            fileSaya << EmpID[i] << +"  " + EmpNama[i] << endl;
        }
    }
}

// ini fungsi Utama
int main()
{
    cout << "MENU\n";
    int options;
    string empID;
    system("clear"); // for linux
    // system("CLS");  //for windows
    membukaFile();

    do
    {
        cout << "================================" << endl;
        cout << "!!=========Program CRUD=======!!" << endl;
        cout << "================================" << endl;
        cout << "\n 1. Membuat Data Mahasiswa";
        cout << "\n 2. Baca Data Mahasiswa";
        cout << "\n 3. Ubah Data Mahasiswa";
        cout << "\n 4. Hapus Data Mahasiswa";
        cout << "\n 5. Mencari Data Mahasiswa";
        cout << "\n 6. Keluar dan Simpan File";
        cout << "\n\n==========================" << endl;

        cout << "Seleksi Pilihan Anda:";
        cin >> options;
        switch (options)
        {
        case 1:
            TambahDataMahasiswa();
            system("clear"); // for linux
            break;
        case 2:
            BacaDataMahasiswa();
            break;
        case 3:
            cin.ignore(); // sehingga datanya tidak 1 di record
            cout << "Mencari data by ID:";
            getline(cin, empID);
            UbahDataMahasiswa(empID);
            break;
        case 4:
            cin.ignore();
            cout << "Delete data by ID:";
            getline(cin, empID);
            HapusDataMahasiswa(empID);
            break;
        case 5:
            cin.ignore(); // sehingga datanya tidak 1 di record
            cout << "Mencari data by ID:";
            getline(cin, empID);
            CariDataMahasiswa(empID);
            break;
        case 6:

        default:
            cout << "Silahkan inputkan pilihan [1 - 6] " << endl;
            break;
        }

    } while (options != 6);
    menyimpanFile();
    cout << "Keluar dan Menyimpan file" << endl;
    return 0;
}
