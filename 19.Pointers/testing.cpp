// ini contoh pointers di bahasa C++
#include <iostream>
using namespace std;

int main()
{
    string minuman = "Jus buah"; // variable dengan tipe string
    string *beverage = &minuman;          // deklarasi pointer
    cout << beverage << endl;             // hasil keluaran makanan "0x7ffe0d903030"
    cout << &beverage << endl;            // alamat memory keluaran dari makanan "Ayam Taliwang"
    cout << *beverage << "\n";            // "cetak ayam taliwang"
    *beverage = "Cendol";                 // ubah nama variable
    cout << *beverage << endl;         // cetak Bakso
    cout << beverage  << endl;          // cetak "0x7ffe0d903030"
}
