#include <iostream>
#include <cstdlib>
#include <sqlite3.h>

using namespace std;
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for (i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    cout << "\n";
    return 0;
}

int main(int argc, char *argv[])
{
    // sqlite3 *db;
    // char *zErrMsg = 0;
    // int rc;
    // char *sql;

    // /* Open database */
    // rc = sqlite3_open("test.db", &db);

    // if (rc)
    // {
    //     cout << "Can't open database:" << sqlite3_errmsg(db) << "\n";
    //     return (0);
    // }
    // else
    // {
    //     cout << "Opened database successfully\n";
    // }

    // /* Create SQL statement */
    // sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
    //       "VALUES (1, 'Varun', 20, 'India', 20000.00 ); "
    //       "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
    //       "VALUES (2, 'John', 25, 'America', 15000.00 ); "
    //       "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)"
    //       "VALUES (3, 'Jack', 23, 'England', 20000.00 );"
    //       "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)"
    //       "VALUES (4, 'Bruce', 25, 'France ', 65000.00 );";

    // /* Execute SQL statement */
    // rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

    // if (rc = SQLITE_OK)
    // {
    //     cout << "SQL error:" << zErrMsg << "\n";
    //     sqlite3_free(zErrMsg);
    // }
    // else
    // {
    //     cout << "Records created successfully\n";
    // }
    // sqlite3_close(db);
    // return 0;

    /* Open database */
    rc = sqlite3_open("test.db", &db);

    if (rc)
    {
        cout << "Can't open database:" << sqlite3_errmsg(db) << "\n";
        return (0);
    }
    else
    {
        cout << "Opened database successfully\n";
    }

    /* Create SQL statement */
    sql = "SELECT * from COMPANY";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *)data, &zErrMsg);
}
