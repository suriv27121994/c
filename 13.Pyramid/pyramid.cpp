#include <iostream>
//#include <curses.h>
#include <stdio.h>
using namespace std;

//main() is where program execution begins.
int main()
{
    int total = 1;
    for (int record = 1; record < 5; record++)
    {
        for (int x = 5; x > record; x--)
        {
            cout << " ";
        }
        for (int y = 1; y <= total; y++)
        {
            cout << "*";
        }
        total += 2;
        cout << "\n";
    }
    //getch();
    getchar();
}

//note:
//tanda petik ("") wajib, jika ('') maka akan error