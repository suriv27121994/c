#include "TesSocket.hpp"

// default constructor
HDE::tesSocket::tesSocket(int domain, int service, int protocol, int port, u_long interface)
{
    // build the connection
    connection = socket(domain, service, protocol);
    // define address structure
    address.sin_family = domain;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = htons(interface);
    test_connection(sock);
    test_connection(connection);
    connection = connect_to_network(sock, address);
}

// Test connection virtual function
void HDE::tesSocket::test_connection(int item_to_test)
{
    // comfirm that the socket established or not
    if (item_to_test < 0)
    {
        perror("failed to connect...");
        exit(EXIT_FAILURE);
    }
}

// The getter function

struct socketaddr_in HDE::tesSocket::get_address()
{
    return address;
}

int HDE::tesSocket::get_sock()
{
    return sock;
}

int HDE::tesSocket::get_connection()
{
    return connection;
}
