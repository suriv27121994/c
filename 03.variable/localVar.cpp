#include <iostream>
using namespace std;

int z;

//ini fungsi testing
void testing()
{
    int abc = 3;
    int xyz = 15;
    z = xyz - abc;

    cout << "ini fungsi testing:" << z << endl;
}

int main()
{
    // Local variable declaration:
    int x, y;

    // actual initialization
    x = 5;
    y = 10;
    z = x + y;

    cout << "This is local variable:" << z << endl;

    testing();

    return 0;
}
