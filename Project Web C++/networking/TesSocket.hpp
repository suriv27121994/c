#ifndef TesSocket_hpp
#define TesSocket_hpp
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

namespace HDE
{
    class tesSocket
    {

    private:
        struct socketaddr_in address;
        int connection;
        int sock;

    public:
        // Constructor
        tesSocket(int domain, int service, int protocol, int port, u_long interface);

        // the virtual function to connect to a network
        virtual int connect_to_network(int sock, struct socketaddr_in address) = 0;
        // the function to test the connection
        void test_connection(int);
        // the getter function
        struct socketaddr_in get_address();
        int get_sock();
        int get_connection();
    };
}

#endif /* socket.hpp */
