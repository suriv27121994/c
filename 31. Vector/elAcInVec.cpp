// element accesser in vector using C++ language
#include <bits/stdc++.h>
using namespace std;

int main()
{
    vector<int> global;

    for (int i = 1; i <= 10; i++)
        global.push_back(i * 10);

    cout << "\nOperator referensi [global] : global[2] = " << global[2];

    cout << "\nat : global.at(4) = " << global.at(4);

    cout << "\nDepan() : global.front() = " << global.front();

    cout << "\nBelakang() : global.back() = " << global.back();

    // pointer ke elemen pertama
    int *pos = global.data();

    cout << "\nElemen pertama adalah " << *pos;
    return 0;
}
