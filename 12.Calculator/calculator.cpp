#include <iostream>  //import input output
using namespace std; //so that function cout, cin and other can be read

int main()
{
    float x, y, result;
    char arithmetic;

    cout << "=============================\n";
    cout << "Welcome to calculator program\n";
    cout << "=============================\n";

label_tes:

    // memasukan input dari user
    cout << "Input first value: ";
    cin >> x;
    cout << "Choice the operators [+, -, /, *]: ";
    cin >> arithmetic;
    cout << "Input secondly value: ";
    cin >> y;

    cout << "Process the calculate :";
    cout << x << arithmetic << y << endl;

    if (arithmetic == '+')
    {
        result = x + y;
    }
    else if (arithmetic == '-')
    {
        result = x - y;
    }
    else if (arithmetic == '/')
    {
        result = x / y;
    }
    else if (arithmetic == '*')
    {
        result = x * y;
    }
    else
    {

        cout << "Your operators is not true" << endl;
        goto label_tes;
    }

    cout << "Result = " << result << endl;
    return 0;
}