#include <iostream> //import input and output streams
#include <stdlib.h>
using namespace std;

//membuat kelas Mahasiswa
class Mahasiswa
{
    int Pilih;
    char Nama[20];
    char Jurusan[20];
    char Fakultas[20];

public:
    int Insert(Mahasiswa *p, int n)
    {
        cout << "Inputkan Pilihan: ";
        cin >> p[n].Pilih;
        cout << "Inputkan Nama: ";
        cin >> p[n].Nama;
        cout << "Inputkan Jurusan: ";
        cin >> p[n].Jurusan;
        cout << "Inputkan Fakultas : ";
        cin >> p[n].Fakultas;
        cout << "\nData Tercatat...\n";
        n++;
        return n;
    }

    void Search(Mahasiswa *p, int roll, int n)
    {
        int i = 0;
        for (i = 0; i < n; i++)
        {
            if (p[i].Pilih == roll)
            {
                cout << "Pilih\tNama\tJurusan\tFakultas\n==============================================\n";
                cout << Pilih << "\t" << Nama << "\t" << Jurusan << "\t" << Fakultas << "\n";
                break;
            }
        }
        if (p[i].Pilih != roll)
        {
            cout << "\nData tidak ditemukan .\n";
        }
    }

    void Display()
    {
        cout << Pilih << "\t" << Nama << "\t" << Jurusan << "\t" << Fakultas << "\n";
    }
    int Del(Mahasiswa *p, int n, int roll)
    {
        int j = 0, k, flag = 0;
        for (j = 0; j < n; j++)
        {
            if (p[j].Pilih == roll)
            {
                flag = 1;
                break;
            }
        }
        if (flag == 1)
        {
            for (k = j; k < n; k++)
            {
                p[k] = p[k + 1];
            }
            cout << "\nData berhasil dihapus.\n";
            return n - 1;
        }
        else
        {
            cout << "\nData tidak ditemukan\n";
            return n;
        }
    }
    int Update(Mahasiswa *p, int roll, int n)
    {
        int i, ch1;
        for (i = 0; i < n; i++)
        {
            if (p[i].Pilih == roll)
            {
                while (1)
                {
                    cout << "\n!!===Pilihan Ubah Data===!!\n";
                    cout << "\n 1. Ubah Jurusan";
                    cout << "\n 2. Ubah Fakultas";
                    cout << "\n 3. Ubah Keduanya";
                    cout << "\n 4. Kembali ke Menu Utama";
                    cout << "\n\n Masukkan Pilihan Anda:";
                    cin >> ch1;
                    switch (ch1)
                    {
                    case 1:
                        cout << "Jurusan:";
                        cin >> p[i].Jurusan;
                        cout << "Data Jurusan berhasil di update...\n";
                        break;
                    case 2:
                        cout << "Fakultas:\t";
                        cin >> p[i].Fakultas;
                        cout << "Data Fakultas berhasil di update...\n";
                        break;
                    case 3:
                        cout << "Jurusan:";
                        cin >> p[i].Jurusan;
                        cout << "Fakultas:\t";
                        cin >> p[i].Fakultas;
                        cout << "Data Jurusan dan Fakultas berhasil di update...\n";
                        break;
                    case 4:
                        return n;
                    default:
                        cout << "!! Inputkan pilihan yang benar !!";
                        break;
                    }
                }
                break;
            }
        }
        if (p[i].Pilih != roll)
        {
            cout << "\nData tidak ditemukan\n\n";
        }
    };
};

int main()
{
    Mahasiswa o[10];
    int i = 0, ch, j, roll;
    while (1)
    {
        cout << "\n!!===Sistem Manajemen Mahasiswa===!!";
        cout << "\n";
        cout << "\n 1.TAMBAH DATA";
        cout << "\n 2.CARI DATA";
        cout << "\n 3.TAMPILKAN DATA";
        cout << "\n 4.HAPUS DATA";
        cout << "\n 5.UBAH DATA";
        cout << "\n 6.KELUAR";
        cout << "\n\n INPUTKAN PILIHAN ANDA:";
        cin >> ch;
        switch (ch)
        {
        case 1:
            i = o[0].Insert(o, i);
            break;
        case 3:
            cout << "PILIHAN\tNAMA\tJURUSAN\tFAKULTAS\n==============================================\n";
            for (j = 0; j < i; j++)
            {
                o[j].Display();
            }
            break;
        case 2:
            cout << "Inputkan pilihan untuk mencari data:";
            cin >> roll;

            o[0].Search(o, roll, i);

            break;
        case 4:
            cout << "Inputkan pilihan untuk menghapus:";
            cin >> roll;
            i = o[0].Del(o, i, roll);
            break;
        case 5:
            cout << "Inputkan pilihan untuk mengupdate data:";
            cin >> roll;
            i = o[0].Update(o, roll, i);
            break;
        default:
            cout << "in";
            break;
        case 6:
            exit(0);
        }
    }
}
