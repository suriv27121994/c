// Modifiers in vector using C++ language
#include <bits/stdc++.h>
#include <vector>
using namespace std;

int main()
{
    // Assign vector
    vector<int> vec;

    // fill the array with 15 ten times
    vec.assign(10, 15);

    cout << "The vector elements are: ";
    for (int i = 0; i < vec.size(); i++)
        cout << vec[i] << " ";

    // inserts 25 to the last position
    vec.push_back(20);
    int n = vec.size();
    cout << "\nThe last element is: " << vec[n - 1];

    // removes last element
    vec.pop_back();

    // prints the vector
    cout << "\nThe vector elements are: ";
    for (int i = 0; i < vec.size(); i++)
        cout << vec[i] << " ";

    // inserts 10 at the beginning
    vec.insert(vec.begin(), 10);

    cout << "\nThe first element is: " << vec[0];

    // removes the first element
    vec.erase(vec.begin());

    cout << "\nThe first element is: " << vec[0];

    // inserts at the beginning
    vec.emplace(vec.begin(), 10);
    cout << "\nThe first element is: " << vec[0];

    // Inserts 25 at the end
    vec.emplace_back(25);
    n = vec.size();
    cout << "\nThe last element is: " << vec[n - 1];

    // erases the vector
    vec.clear();
    cout << "\nVector size after erase(): " << vec.size();

    // two vector to perform swap
    vector<int> v1, v2;
    v1.push_back(1);
    v1.push_back(2);
    v2.push_back(3);
    v2.push_back(4);

    cout << "\n\nVector 1: ";
    for (int i = 0; i < v1.size(); i++)
        cout << v1[i] << " ";

    cout << "\nVector 2: ";
    for (int i = 0; i < v2.size(); i++)
        cout << v2[i] << " ";

    // Swaps v1 and v2
    v1.swap(v2);

    cout << "\nAfter Swap \nVector 1: ";
    for (int i = 0; i < v1.size(); i++)
        cout << v1[i] << " ";

    cout << "\nVector 2: ";
    for (int i = 0; i < v2.size(); i++)
        cout << v2[i] << " ";
}
