#include <iostream>
using namespace std;

class Kotak
{
public:
    double panjang;  // Panjang box
    double luas; // Luas box
    double tinggi;  // Tingg box
};

int main()
{
    Kotak Kotak1;            // Deklarasi kotak 1 dari tipe Kotak
    Kotak Kotak2;            // Deklarasi kotak 2 dari tipe Kotak
    double volume = 0.0;   // Simpan volume kotak di sini

    // Spesifikasi kotak 1
    Kotak1.tinggi = 7.0;
    Kotak1.panjang = 5.0;
    Kotak1.luas = 9.0;

    // Spesifikasi kotak 2
    Kotak2.tinggi = 10.0;
    Kotak2.panjang = 15.0;
    Kotak2.luas = 13.0;

    // volume kotak 1
    volume = Kotak1.tinggi * Kotak1.panjang * Kotak1.luas;
    cout << "Volume Kotak 1 : " << volume << endl;

    // volume kotak 2
    volume = Kotak2.tinggi * Kotak2.panjang * Kotak2.luas;
    cout << "Volume Kotak 2 : " << volume << endl;

    return 0;
}