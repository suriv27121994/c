// ini contoh pointers di bahasa C++
#include <iostream>
using namespace std;

int main()
{
    // cara pertama deklarasi variable
    // int myPointer = 15;

    // cara kedua
    int myPointer;
    myPointer = 15;

    // pointer & untuk referensi alamat memori
    cout << &myPointer << endl;

    // deklarasikan variable dereferensi
    int *pointer;
    pointer = &myPointer;
    cout << *pointer;
}
