#include <iostream>
using namespace std;
int main()
{
    int bilangan;
    long faktorial = 1;
    
    cout << "Masukkan sesuatu bilangan: ";
    cin >> bilangan;
    cout << endl;
    cout << bilangan << "! = ";
    while (bilangan >= 1)
    {
        faktorial = faktorial * bilangan;
        if (bilangan != 1)
        {
            cout << bilangan << " x ";
        }
        else
        {
            cout << bilangan << " = ";
        }
        bilangan--;
    }
    cout << faktorial << endl;
    return 0;
}