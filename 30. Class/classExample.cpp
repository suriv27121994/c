#include <iostream>
using namespace std;

class ThisClass
{                    // The class
public:              // Access specifier
    int myNum;       // Attribute (int variable)
    string myString; // Attribute (string variable)
};

int main()
{
    ThisClass myObj; // Create an object of MyClass

    // Access attributes and set values
    myObj.myNum = 100;
    myObj.myString = "this is string";

    // Print attribute values
    cout << myObj.myNum << "\n";
    cout << myObj.myString << "\n";
    return 0;
}