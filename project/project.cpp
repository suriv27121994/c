#include <iostream>
#include <fstream>
#include <string>
#include <limits> //for max

using namespace std;

int getOption();
void simpanData();
void bacaData();
void ubahData(int nimDicari);
void hapusData(int nimDicari);
void searchData(int nimDicari);

int main()
{
    int pilihan;

    char is_continue;
    char cariData;

    enum option
    {
        CREATE = 1,
        READ,
        UPDATE, // TODO
        DELETE,
        SEARCH,
        FINISH
    };

    while (pilihan != FINISH)
    {

        switch (pilihan)
        {
        case CREATE:
            cout << "Menambah data mahasiswa" << endl;
            simpanData();
            break;
        case READ:
            cout << "Tampilkan data mahasiswa" << endl;
            bacaData();
            break;
        case UPDATE:
            cout << "Ubah data mahasiswa" << endl;
            cout << "\nInputkan Nim Mahasiswa:";
            cin >> cariData;
            ubahData(cariData);
            break;
        case DELETE:
            cout << "Hapus data mahasiswa" << endl;
            cout << "\nInputkan Nim Mahasiswa:";
            cin >> cariData;
            hapusData(cariData);
            break;
        case SEARCH:
            cout << "\nSearch data by Nim" << endl;
            cout << "\nInputkan Nim Mahasiswa:";
            cin >> cariData;
            searchData(cariData);
        default:
            cout << "Pilihan tidak ditemukan" << endl;
            break;
        }

    label_continue:

        cout << "Lanjutkan?(y/n) : ";
        cin >> is_continue;
        if ((is_continue == 'y') | (is_continue == 'Y'))
        {
            pilihan = getOption();
        }
        else if ((is_continue == 'n') | (is_continue == 'N'))
        {
            break;
        }
        else
        {
            goto label_continue;
        }
    }

    cout << "akhir dari program" << endl;

    cin.get();
    return 0;
}

int getOption()
{
    int input;
    system("clear");
    // system("cls");
    cout << "\nProgram CRUD data mahasiswa" << endl;
    cout << "=============================" << endl;
    cout << "1. Tambah data mahasiswa" << endl;
    cout << "2. Tampilkan data mahasiswa" << endl;
    cout << "3. Ubah data mahasiswa" << endl;
    cout << "4. Hapus data mahasiswa" << endl;
    cout << "5. Selesai" << endl;
    cout << "=============================" << endl;
    cout << "pilih [1-5]? : ";
    cin >> input;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    return input;
}

void simpanData()
{
    float NilaiMahasiswa, nilai;
    nilai.inputNilai();
    //simpan objek ke file
    ofstream f;
    f.open(namaFile, ios::binary | ios::app);
    f.write((char *)&nilai, sizeof(nilai));
    f.close();
}

void bacaData()
{
    NilaiMahasiswa nilai;
    ifstream f;
    f.open(namaFile, ios::binary);
    cout << "NIM\t"
         << "Nama\t"
         << "Tugas\t"
         << "UTS\t"
         << "UAS\t"
         << "Nilai";
    cout << "Akhir\t"
         << "Nilai Huruf\t" << endl;
    while (f.read((char *)&nilai, sizeof(nilai)))
    {
        nilai.outputNilai();
    }
    f.close();
}

void ubahData(int nimDicari)
{
    NilaiMahasiswa nilai;
    fstream f;
    f.open(namaFile, ios::in | ios::out);
    while (f.read((char *)&nilai, sizeof(nilai)))
    {
        if (nilai.getNim() == nimDicari)
        {
            //tampil data sebelum update
            cout << "NIM\t"
                 << "Nama\t"
                 << "Tugas\t"
                 << "UTS\t"
                 << "UAS\t";
            cout << "Nilai Akhir\t"
                 << "Nilai Huruf\t" << endl;
            nilai.outputNilai();
            //input data baru
            cout << "Silahkan masukkan data baru" << endl;
            nilai.inputNilai();

            int pos = -1 * sizeof(nilai);
            f.seekp(pos, ios::cur);
            f.write((char *)&nilai, sizeof(nilai));
            break;
        }
    }
    f.close();
}

void hapusData(int nimDicari)
{
    NilaiMahasiswa nilai;
    ifstream fi;
    fi.open(namaFile, ios::binary);
    ofstream fo;
    fo.open("tmp.dat", ios::out | ios::binary);
    int ketemu = 0;
    while (fi.read((char *)&nilai, sizeof(nilai)))
    {
        if (nilai.getNim() != nimDicari)
        {
            fo.write((char *)&nilai, sizeof(nilai));
        }
        else
        {
            ketemu = 1;
        }
    }
    fi.close();
    fo.close();

    remove(namaFile);
    rename("tmp.dat", namaFile);
    if (ketemu == 1)
    {
        cout << "Data dengan NIM " << nimDicari << " telah dihapus!" << endl;
    }
    else
    {
        cout << "Data dengan NIM " << nimDicari << " Tidak ditemukan" << endl;
    }
}

void searchData(int nimDicari)
{
    float NilaiMahasiswa, nilai, nilaiketemu;

    int ketemu = 0;
    ifstream f;
    f.open(namaFile, ios::binary);
    while (f.read((char *)&nilai, sizeof(nilai)))
    {
        if (nilai.getNim() == nimDicari)
        {
            nilaiketemu = nilai;
            ketemu = 1;
            break;
        }
    }
    if (ketemu == 0)
    {
        cout << "\nNim " << nimDicari << " tidak ditemukan" << endl;
    }
    else
    {
        cout << "\nNim " << nimDicari << " ditemukan" << endl
             << endl;
        cout << "NIM\t"
             << "Nama\t"
             << "Tugas\t"
             << "UTS\t"
             << "UAS\t";
        cout << "Nilai Akhir\t"
             << "Nilai Huruf\t" << endl;
        nilaiketemu.outputNilai();
    }
    f.close();
}
