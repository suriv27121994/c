#include <iostream>
using namespace std;

int main()
{

    // deklarasi variable
    float alas, tinggi;
    float luas;

    // algorithm
    // Inputan alas dan tinggi
    cout << "Masukkan panjang alas: ";
    cin >> alas;
    cout << "Masukkan tinggi segitiga: ";
    cin >> tinggi;

    luas = 0.5 * alas * tinggi;
    cout << "Luas segitiga adalah " << luas << endl;

    return 0;
}