#include <iostream>
using namespace std;

int main(){
    int x = 25;
    int* p = &x;

    // cout << x << endl;
    // cout << p << endl;
    // cout << *p << endl;
    x = x + 5;
    x = *p + 5;
    *p = *p + 5;
    
    cout << x << endl;
    cout << *p << endl;
    cout << &*p << endl;
}