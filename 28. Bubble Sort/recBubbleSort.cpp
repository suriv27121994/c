#include <iostream>
using namespace std;
void recBubble(int arr[], int n)
{
    if (n == 1)
        return;
    for (int x = 0; x < n - 1; x++)   // for each pass p
        if (arr[x] > arr[x + 1])      // if the current element is greater than next one
            swap(arr[x], arr[x + 1]); // swap elements
    recBubble(arr, n - 1);
}

// main function
int main()
{
    int data[] = {100, 50, 8, 300, 90, 50, 10, 15, 20, 10};
    int n = sizeof(data) / sizeof(data[0]);
    cout << "SORTED ARRAY IS: ";
    recBubble(data, n);
    for (int y = 0; y < n; y++)
    {
        cout << data[y] << " ";
    }
}