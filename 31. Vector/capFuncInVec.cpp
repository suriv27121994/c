// capacity function in vector using the C++ language
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> global;

    for (int i = 1; i <= 5; i++)
        global.push_back(i);

    cout << "Ukuran : " << global.size();
    cout << "\nKapasitas : " << global.capacity();
    cout << "\nUkuran Maksimal : " << global.max_size();

    // mengubah ukuran vektor menjadi 4
    global.resize(4);

    // mencetak ukuran vektor setelah mengubah ukuran()
    cout << "\nUkuran : " << global.size();

    // checks if the vector is empty or not
    if (global.empty() == false)
        cout << "\nVektor tidak kosong";
    else
        cout << "\nVektor kosong";

    // Mengecilkan vektor
    global.shrink_to_fit();
    cout << "\nElemen vektor adalah: ";
    for (auto it = global.begin(); it != global.end(); it++)
        cout << *it << " ";

    return 0;
}
