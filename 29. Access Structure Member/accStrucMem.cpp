#include <iostream>
#include <cstring>

using namespace std;

struct Buku
{
    int id_buku;
    char judul[350];
    char penulis[35];
    char mataPelajaran[500];
    
};

int main()
{
    struct Buku Buku1; // Deklarasi buku1 pada jenis Buku
    struct Buku Buku2; // Deklarasi buku2 pada jenis Buku

    // Spesefikasi Buku 1
    strcpy(Buku1.judul, "OOP C++ Programming");
    strcpy(Buku1.penulis, "Gusti Arsyad");
    strcpy(Buku1.mataPelajaran, "C++ Programming");
    Buku1.id_buku = 1903;

    // Spesefikasi Buku 2
    strcpy(Buku2.judul, "What is Cryptography?");
    strcpy(Buku2.penulis, "Andre");
    strcpy(Buku2.mataPelajaran, "Cryptography");
    Buku2.id_buku = 2005;

    // Print Buku1 info
    cout << "Judul Buku 1 : " << Buku1.judul << endl;
    cout << "Penulis Buku 1 : " << Buku1.penulis << endl;
    cout << "Mata Pelajaran Buku 1: " << Buku1.mataPelajaran << endl;
    cout << "Id Buku 1 : " << Buku1.id_buku << endl;

    // Print Buku 2 info
    cout << "Judul Buku 2 : " << Buku2.judul << endl;
    cout << "Penulis Buku 2 : " << Buku2.penulis << endl;
    cout << "Mata Pelajaran Buku 2: " << Buku2.mataPelajaran << endl;
    cout << "Id Buku 2 : " << Buku2.id_buku << endl;
    return 0;
}