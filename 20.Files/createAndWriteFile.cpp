#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    // create and open a text file
    ofstream MyFile("createFile.txt");

    // Write to the file
    MyFile << "Hello File, this testing!";

    // close the file
    MyFile.close();

    return 0;
}