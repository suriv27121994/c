// pointer to functions
#include <iostream>
using namespace std;

int tambah(int x, int y)
{
    return (x + y);
}

int kurang(int x, int y)
{
    return (x - y);
}

int operation(int a, int b, int (*functocall)(int, int))
{
    int g;
    g = (*functocall)(a, b);
    return (g);
}

int main()
{
    int m, n;
    int (*minus)(int, int) = kurang;

    m = operation(3, 1, tambah);
    cout << m << endl;
    n = operation(10, m, minus);
    cout << n << endl;
    return 0;
}

/*
output:
15
10
*/