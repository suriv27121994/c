#include <iostream>
using namespace std;

int main(){
    int a;
    a = 14;
    cout << &a << endl;

    int* p;
    p = &a;
    cout << p << endl;

    *p = 10;
    cout << *p << endl;
}