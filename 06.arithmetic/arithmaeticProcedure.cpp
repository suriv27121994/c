#include <iostream>  //import input output
using namespace std; //so that function cout, cin and other can be read

int a = 20;
int b = 10;
int result;

void tambah()
{
    result = a + b;
    cout << a << " + " << b << " = " << result << endl;
}

void kurang()
{
    // subtraction
    result = a - b;
    cout << a << " - " << b << " = " << result << endl;
}

void bagi()
{
    // distribution
    result = a / b;
    cout << a << " / " << b << " = " << result << endl;
}

void kali()
{
    // multiplication
    result = a * b;
    cout << a << " x " << b << " = " << result << endl;
}

void modulus()
{
    // modulus
    result = a % b;
    cout << a << " % " << b << " = " << result << endl;
}

int main() //main function
{
    tambah();
    kurang();
    kali();
    bagi();
    //modulus();

        // operatornya +, -, *, /, %
        // addition

        // order of execution;
        // result = a + b * a;
        // cout << result << endl;

        //cin.get();
        return 0;
}