#include <iostream>
using namespace std;

void SWAP(int &pertama, int &kedua)
{
    int temporary = pertama;
    pertama = kedua;
    kedua = temporary;
}

int main()
{
    int abc = 10, xyz = 15;
    SWAP(abc, xyz);
    cout << abc << " " << xyz;
    return 0;
}
