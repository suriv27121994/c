// iterators in vector using C++
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> global;

    for (int i = 1; i <= 5; i++)
        global.push_back(i);

    cout << "Output dari start dan end: ";
    for (auto i = global.begin(); i != global.end(); ++i)
        cout << *i << " ";

    cout << "\nOutput dari cstart dan cend: ";
    for (auto i = global.cbegin(); i != global.cend(); ++i)
        cout << *i << " ";

    cout << "\nOutput dari rstart dan rend: ";
    for (auto ir = global.rbegin(); ir != global.rend(); ++ir)
        cout << *ir << " ";

    cout << "\nOutput dari crstart dan crend: ";
    for (auto ir = global.crbegin(); ir != global.crend(); ++ir)
        cout << *ir << " ";

    return 0;
}
