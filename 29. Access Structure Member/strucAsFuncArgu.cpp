#include <iostream>
#include <cstring>

using namespace std;
void printBuku(struct Buku buku);

struct Buku
{
    int id_buku;
    char judul[350];
    char penulis[35];
    char mataPelajaran[500];
};

int main()
{
    struct Buku Buku1; // Deklarasi buku1 pada jenis Buku
    struct Buku Buku2; // Deklarasi buku2 pada jenis Buku

    // Spesefikasi Buku 1
    strcpy(Buku1.judul, "OOP C++ Programming");
    strcpy(Buku1.penulis, "Gusti Arsyad");
    strcpy(Buku1.mataPelajaran, "C++ Programming");
    Buku1.id_buku = 1903;

    // Spesefikasi Buku 2
    strcpy(Buku2.judul, "What is Cryptography?");
    strcpy(Buku2.penulis, "Andre");
    strcpy(Buku2.mataPelajaran, "Cryptography");
    Buku2.id_buku = 2005;

    // Print info Buku 1
    printBuku(Buku1);

    //// Print info Buku 1
    printBuku(Buku2);

    return 0;
}
void printBuku(struct Buku buku)
{
    cout << "Judul Buku : " << buku.judul << endl;
    cout << "Penulis Buku : " << buku.penulis << endl;
    cout << "Buku Mata Pelajaran : " << buku.mataPelajaran << endl;
    cout << "Id buku : " << buku.id_buku << endl;
}