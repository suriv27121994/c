#include <iostream>
using namespace std;

// Global variable declaration:
int g; //g mean that global
int a; //a mean that global
int b; //b mean that global
int ab;


//this function kurang
void kurang()
{
    int z = 10;
    int x;
    
    x = 20;
    b = 5;
    ab = a - b;
    cout << ab << endl;
}

//this function kali
void kali()
{

    a = 20;
    b = 5;
    ab = a * b;
    cout << ab << endl;
}

int main()
{

    // Local variable declaration:
    int x, y;

    // actual initialization
    x = 5;
    y = 10;
    g = x + y;

    cout << "This is global variable:" << g << endl;

    //ini fungsi kali dan kurang
    kali();
    kurang();

    return 0;
}
